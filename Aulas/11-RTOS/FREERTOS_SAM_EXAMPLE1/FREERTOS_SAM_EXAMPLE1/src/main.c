#include <asf.h>
#include "conf_board.h"

#define configASSERT_DEFINED 1

#define TASK_MONITOR_STACK_SIZE            (2048/sizeof(portSTACK_TYPE))
#define TASK_MONITOR_STACK_PRIORITY        (tskIDLE_PRIORITY)
#define TASK_LED_STACK_SIZE                (1024/sizeof(portSTACK_TYPE))
#define TASK_LED_STACK_PRIORITY            (tskIDLE_PRIORITY)

extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName);
extern void vApplicationIdleHook(void);
extern void vApplicationTickHook(void);
extern void vApplicationMallocFailedHook(void);
extern void xPortSysTickHandler(void);

/* Botao da placa */
#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

#define LEDX1_PIO_ID ID_PIOA
#define LEDX1_PIO PIOA
#define LEDX1_PIN 0
#define LEDX1_PIN_MASK (1 << LEDX1_PIN)

#define LEDX2_PIO_ID ID_PIOC
#define LEDX2_PIO PIOC
#define LEDX2_PIN 30
#define LEDX2_PIN_MASK (1 << LEDX2_PIN)

#define LEDX3_PIO_ID ID_PIOB
#define LEDX3_PIO PIOB
#define LEDX3_PIN 2
#define LEDX3_PIN_MASK (1 << LEDX3_PIN)

#define BUTX1_PIO_ID ID_PIOD
#define BUTX1_PIO PIOD
#define BUTX1_PIN 28
#define BUTX1_PIN_MASK (1 << BUTX1_PIN)

#define BUTX2_PIO_ID ID_PIOC
#define BUTX2_PIO PIOC
#define BUTX2_PIN 31
#define BUTX2_PIN_MASK (1 << BUTX2_PIN)

#define BUTX3_PIO_ID ID_PIOA
#define BUTX3_PIO PIOA
#define BUTX3_PIN 19
#define BUTX3_PIN_MASK (1 << BUTX3_PIN)

void but_callback(void);
void init_but_board(void);

/** Semaforo a ser usado pela task led */
SemaphoreHandle_t xSemaphore;
SemaphoreHandle_t xSemaphoreLedX1;
SemaphoreHandle_t xSemaphoreLedX2;
SemaphoreHandle_t xSemaphoreLedX3;

/**
 * \brief Called if stack overflow during execution
 */
extern void vApplicationStackOverflowHook(xTaskHandle *pxTask,
		signed char *pcTaskName)
{
	printf("stack overflow %x %s\r\n", pxTask, (portCHAR *)pcTaskName);
	/* If the parameters have been corrupted then inspect pxCurrentTCB to
	 * identify which task has overflowed its stack.
	 */
	for (;;) {
	}
}

/**
 * \brief This function is called by FreeRTOS idle task
 */
extern void vApplicationIdleHook(void)
{
	pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
}

/**
 * \brief This function is called by FreeRTOS each tick
 */
extern void vApplicationTickHook(void)
{
}

extern void vApplicationMallocFailedHook(void)
{
	/* Called if a call to pvPortMalloc() fails because there is insufficient
	free memory available in the FreeRTOS heap.  pvPortMalloc() is called
	internally by FreeRTOS API functions that create tasks, queues, software
	timers, and semaphores.  The size of the FreeRTOS heap is set by the
	configTOTAL_HEAP_SIZE configuration constant in FreeRTOSConfig.h. */

	/* Force an assert. */
	configASSERT( ( volatile void * ) NULL );
}

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

/**
 * \brief This task, when activated, send every ten seconds on debug UART
 * the whole report of free heap and total tasks status
 */
static void task_monitor(void *pvParameters)
{
	static portCHAR szList[256];
	UNUSED(pvParameters);

	for (;;) {
		printf("--- task ## %u", (unsigned int)uxTaskGetNumberOfTasks());
		vTaskList((signed portCHAR *)szList);
		printf(szList);
		vTaskDelay(1000);
	}
}

/**
 * \brief This task, when activated, make LED blink at a fixed rate
 */
static void task_led(void *pvParameters)
{
	/* Attempt to create a semaphore. */
	xSemaphore = xSemaphoreCreateBinary();

	if (xSemaphore == NULL)
		printf("falha em criar o semaforo \n");

	for (;;) {
		if( xSemaphoreTake(xSemaphore, ( TickType_t ) 500) == pdTRUE ){
			LED_Toggle(LED0);
			
		}
	}
}

static void task_led_XPRO1(void *pvParameters)
{
	/* Block for 500ms. */
	const TickType_t xDelay = 125 / portTICK_PERIOD_MS;
	
	volatile bool isToggling = true;

	/* Attempt to create a semaphore. */
	xSemaphoreLedX1 = xSemaphoreCreateBinary();

	if (xSemaphoreLedX1 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if(isToggling){
			pin_toggle(LEDX1_PIO, LEDX1_PIN_MASK);
			vTaskDelay(xDelay);
		}
		if( xSemaphoreTake(xSemaphoreLedX1, ( TickType_t ) 0) == pdTRUE ){
			if(isToggling){
				isToggling = false;
				pio_set(LEDX1_PIO, LEDX1_PIN_MASK);
			}
			else{
				isToggling = true;
			}
			//pin_toggle(LEDX1_PIO, LEDX1_PIN_MASK);
		}
	}
}

static void task_led_XPRO2(void *pvParameters)
{
	/* Block for 500ms. */
	const TickType_t xDelay = 500 / portTICK_PERIOD_MS;
	
	volatile bool isToggling = true;

	/* Attempt to create a semaphore. */
	xSemaphoreLedX2 = xSemaphoreCreateBinary();

	if (xSemaphoreLedX2 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if(isToggling){
			pin_toggle(LEDX2_PIO, LEDX2_PIN_MASK);
			vTaskDelay(xDelay);
		}
		if( xSemaphoreTake(xSemaphoreLedX2, ( TickType_t ) 0) == pdTRUE ){
			if(isToggling){
				isToggling = false;
				pio_set(LEDX2_PIO, LEDX2_PIN_MASK);
			}
			else{
				isToggling = true;
			}
		}
	}
}

static void task_led_XPRO3(void *pvParameters)
{
	/* Block for 500ms. */
	const TickType_t xDelay = 50 / portTICK_PERIOD_MS;
	
	volatile bool isToggling = true;

	/* Attempt to create a semaphore. */
	xSemaphoreLedX3 = xSemaphoreCreateBinary();

	if (xSemaphoreLedX3 == NULL)
	printf("falha em criar o semaforo \n");

	for (;;) {
		if(isToggling){
			pin_toggle(LEDX3_PIO, LEDX3_PIN_MASK);
			vTaskDelay(xDelay);
		}
		if( xSemaphoreTake(xSemaphoreLedX3, ( TickType_t ) 0) == pdTRUE ){
			if(isToggling){
				isToggling = false;
				pio_set(LEDX3_PIO, LEDX3_PIN_MASK);
			}
			else{
				isToggling = true;
			}
		}
	}
}

void but_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("but_callback \n");
	xSemaphoreGiveFromISR(xSemaphore, &xHigherPriorityTaskWoken);
	printf("semafaro tx \n");
}

void ButtonX1_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("butX1_callback \n");
	xSemaphoreGiveFromISR(xSemaphoreLedX1, &xHigherPriorityTaskWoken);
	printf("semafaro Ledx1\n");
	
}

void ButtonX2_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("butX2_callback \n");
	xSemaphoreGiveFromISR(xSemaphoreLedX2, &xHigherPriorityTaskWoken);
	printf("semafaro Ledx2\n");
	
}

void ButtonX3_callback(void){
	BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	printf("butX3_callback \n");
	xSemaphoreGiveFromISR(xSemaphoreLedX3, &xHigherPriorityTaskWoken);
	printf("semafaro Ledx3\n");
	
}


void init_but_board(void){
	/* conf bot�o como entrada */
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 8);
	pio_configure(BUT_PIO, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);
	pio_set_debounce_filter(BUT_PIO, BUT_PIO_PIN_MASK, 60);
	pio_enable_interrupt(BUT_PIO, BUT_PIO_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN_MASK, PIO_IT_FALL_EDGE , but_callback);
}


void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE, handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 2);
};

void LEDX_init(){
	pio_set_output(LEDX1_PIO, LEDX1_PIN_MASK, 1, 0, 0);
	pio_set_output(LEDX2_PIO, LEDX2_PIN_MASK, 1, 0, 0);
	pio_set_output(LEDX3_PIO, LEDX3_PIN_MASK, 1, 0, 0);
};

/**
 * \brief Configure the console UART.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
#if (defined CONF_UART_CHAR_LENGTH)
		.charlength = CONF_UART_CHAR_LENGTH,
#endif
		.paritytype = CONF_UART_PARITY,
#if (defined CONF_UART_STOP_BITS)
		.stopbits = CONF_UART_STOP_BITS,
#endif
	};

	/* Configure console UART. */
	stdio_serial_init(CONF_UART, &uart_serial_options);

	/* Specify that stdout should not be buffered. */
#if defined(__GNUC__)
	setbuf(stdout, NULL);
#else
	/* Already the case in IAR's Normal DLIB default configuration: printf()
	 * emits one character at a time.
	 */
#endif
}

/**
 *  \brief FreeRTOS Real Time Kernel example entry point.
 *
 *  \return Unused (ANSI-C compatibility).
 */
int main(void)
{
	/* Initialize the SAM system */
	sysclk_init();
	board_init();

	/* Initialize the console uart */
	configure_console();

	/* Output demo information. */
	printf("-- Freertos Example --\n\r");
	printf("-- %s\n\r", BOARD_NAME);
	printf("-- Compiled: %s %s --\n\r", __DATE__, __TIME__);

	/* iniciliza botao */
	init_but_board();
	
	//Inicializa botoes da Xpro
	BUTX_init(BUTX1_PIO, BUTX1_PIO_ID, BUTX1_PIN, BUTX1_PIN_MASK, ButtonX1_callback);
	BUTX_init(BUTX2_PIO, BUTX2_PIO_ID, BUTX2_PIN, BUTX2_PIN_MASK, ButtonX2_callback);
	BUTX_init(BUTX3_PIO, BUTX3_PIO_ID, BUTX3_PIN, BUTX3_PIN_MASK, ButtonX3_callback);
	
	//Inicializa Leds da xpro
	LEDX_init();
	

	/* Create task to monitor processor activity */
	if (xTaskCreate(task_monitor, "Monitor", TASK_MONITOR_STACK_SIZE, NULL,
			TASK_MONITOR_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create Monitor task\r\n");
	}

	/* Create task to make led blink */
	if (xTaskCreate(task_led, "Led", TASK_LED_STACK_SIZE, NULL,
			TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led_XPRO1, "LedX1", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led_XPRO2, "LedX2", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}
	
	if (xTaskCreate(task_led_XPRO3, "LedX3", TASK_LED_STACK_SIZE, NULL,
	TASK_LED_STACK_PRIORITY, NULL) != pdPASS) {
		printf("Failed to create test led task\r\n");
	}

	/* Start the scheduler. */
	vTaskStartScheduler();

	/* Will only get here if there was insufficient memory to create the idle task. */
	return 0;
}
