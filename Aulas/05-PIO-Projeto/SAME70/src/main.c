/**
 * 5 semestre - Eng. da Computao - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"

#define LED_PIO PIOC
#define LED_PIO_ID 12
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)

#define BUT_PIO PIOA
#define BUT_PIO_ID 10
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)



/**
 * \brief Set a high output level on all the PIOs defined in ul_mask.
 * This has no immediate effects on PIOs that are not output, but the PIO
 * controller will save the value if they are changed to outputs.
 *
 * \param p_pio Pointer to a PIO instance.
 * \param ul_mask Bitmask of one or more pin(s) to configure.
 */
void _pio_set(Pio *p_pio, const uint32_t ul_mask){
	p_pio->PIO_SODR = ul_mask;
}


/**
 * \brief Set a low output level on all the PIOs defined in ul_mask.
 * This has no immediate effects on PIOs that are not output, but the PIO
 * controller will save the value if they are changed to outputs.
 *
 * \param p_pio Pointer to a PIO instance.
 * \param ul_mask Bitmask of one or more pin(s) to configure.
 */
void _pio_clear(Pio *p_pio, const uint32_t ul_mask){
	p_pio->PIO_CODR = ul_mask;
}


/**
 * \brief Configure PIO internal pull-up.
 *
 * \param p_pio Pointer to a PIO instance.
 * \param ul_mask   of one or more pin(s) to configure.
 * \param ul_pull_up_enable Indicates if the pin(s) internal pull-up shall be
 * configured.
 */
void _pio_pull_up(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_pull_up_enable){
	if(ul_pull_up_enable == 1){
		p_pio->PIO_PUER = ul_mask;
	}
	else{
		p_pio->PIO_PUDR = ul_mask;
	}
}


/**
 * \brief Configure one or more pin(s) of a PIO controller as outputs, with
 * the given default value. Optionally, the multi-drive feature can be enabled
 * on the pin(s).
 *
 * \param p_pio Pointer to a PIO instance.
 * \param ul_mask Bitmask indicating which pin(s) to configure.
 * \param ul_default_level Default level on the pin(s).
 * \param ul_multidrive_enable Indicates if the pin(s) shall be configured as
 * open-drain.
 * \param ul_pull_up_enable Indicates if the pin shall have its pull-up
 * activated.
 */
void _pio_set_output(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_default_level, const uint32_t ul_multidrive_enable, const uint32_t ul_pull_up_enable){
	p_pio->PIO_PER = ul_mask;
	p_pio->PIO_OER = ul_mask;
	
	if(ul_default_level){
		_pio_set(p_pio, ul_mask);
	} else{
		_pio_clear(p_pio, ul_mask);
	}
	
	if(ul_multidrive_enable){
		p_pio->PIO_MDER = ul_mask;
	} else{
		p_pio->PIO_MDDR = ul_mask;
	}
	
	_pio_pull_up(p_pio, ul_mask, ul_pull_up_enable);
}


/*  Default pin configuration (no attribute). */
#define _PIO_DEFAULT             (0u << 0)
/*  The internal pin pull-up is active. */
#define _PIO_PULLUP              (1u << 0)
/*  The internal glitch filter is active. */
#define _PIO_DEGLITCH            (1u << 1)
/*  The pin is open-drain. */
#define _PIO_OPENDRAIN           (1u << 2)
/*  The internal debouncing filter is active. */
#define _PIO_DEBOUNCE            (1u << 3)


/**
 * \brief Configure one or more pin(s) or a PIO controller as inputs.
 * Optionally, the corresponding internal pull-up(s) and glitch filter(s) can
 * be enabled.
 *
 * \param p_pio Pointer to a PIO instance.
 * \param ul_mask Bitmask indicating which pin(s) to configure as input(s).
 * \param ul_attribute PIO attribute(s).
 */
void _pio_set_input(Pio *p_pio, const uint32_t ul_mask, const uint32_t ul_attribute){
	p_pio -> PIO_ODR = ul_mask;
	if(ul_attribute & _PIO_DEFAULT){
		continue;
	}
	if(ul_attribute & _PIO_PULLUP){
		_pio_pull_up(p_pio, ul_mask, 1);
	}
	if((ul_attribute & _PIO_DEGLITCH) != 0){
		p_pio -> PIO_IFSCDR = ul_mask;
	}
	if((ul_attribute & _PIO_OPENDRAIN) != 0){
		p_pio ->PIO_MDER = ul_mask;
	}
	if((ul_attribute & _PIO_DEBOUNCE) != 0){
		p_pio -> PIO_IFSCER = ul_mask;
	}
}


// Funcao principal chamada na inicalizacao do uC.
int main(void){
	
	//board clock
	sysclk_init();
	
	//Desliga watchdog
	WDT->WDT_MR = WDT_MR_WDDIS;

	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(BUT_PIO_ID);
	
	//pio_configure(PIOC, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	_pio_set_output(LED_PIO, LED_PIO_PIN_MASK, 0, 0, 1);	
	pio_configure(PIOA, PIO_INPUT, BUT_PIO_PIN_MASK, PIO_DEFAULT);
    _pio_pull_up(BUT_PIO, BUT_PIO_PIN_MASK, 1);
	
	// super loop
	// aplicacoes embarcadas n�o devem sair do while(1).
	
while (1) {
	if(!pio_get(PIOA, PIO_INPUT, BUT_PIO_PIN_MASK)){
		for(int i = 0; i < 5; i++){
			//Set LED
			_pio_set(PIOC, LED_PIO_PIN_MASK);
			delay_ms(200);
			//Clear LED
			_pio_clear(PIOC, LED_PIO_PIN_MASK);
			delay_ms(200);
		}
	}
	else {
		//Clear LED
		_pio_set(PIOC, LED_PIO_PIN_MASK);
	}
	
	
}
	return 0;
}

