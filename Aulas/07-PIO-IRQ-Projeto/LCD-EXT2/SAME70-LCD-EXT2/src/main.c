#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

#define LED_PIO PIOC
#define LED_PIO_ID ID_PIOC
#define LED_PIO_PIN 8
#define LED_PIO_PIN_MASK (1 << LED_PIO_PIN)					

#define BUT_PIO PIOA
#define BUT_PIO_ID ID_PIOA
#define BUT_PIO_PIN 11
#define BUT_PIO_PIN_MASK (1 << BUT_PIO_PIN)

#define BUTX1_PIO_ID ID_PIOD
#define BUTX1_PIO PIOD
#define BUTX1_PIN 28
#define BUTX1_PIN_MASK (1 << BUTX1_PIN)

#define LEDX1_PIO_ID ID_PIOA
#define LEDX1_PIO PIOA
#define LEDX1_PIN 0
#define LEDX1_PIN_MASK (1 << LEDX1_PIN)

#define LEDX2_PIO_ID ID_PIOC
#define LEDX2_PIO PIOC
#define LEDX2_PIN 30
#define LEDX2_PIN_MASK (1 << LEDX2_PIN)

#define LEDX3_PIO_ID ID_PIOB
#define LEDX3_PIO PIOB
#define LEDX3_PIN 2
#define LEDX3_PIN_MASK (1 << LEDX3_PIN)

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL
	
struct ili9488_opt_t g_ili9488_display_opt;
void pisca_led(void);
void write_freq(uint32_t freq);
void ButtonX_Handler(void);
void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void));
void pin_toggle(Pio *pio, uint32_t mask);


/**
}
/**
 * \brief Configure UART console.
 */
static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

volatile Bool but_flag = false;
volatile Bool butX_flag= false;
volatile Bool flag_led0= 1;
volatile uint32_t freq = 1;

void ButtonX_Handler(void){
	butX_flag = true;
}

void TC1_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_led0)
		pin_toggle(LED_PIO, LED_PIO_PIN_MASK);
		//pin_toggle(LEDX1_PIO, LEDX1_PIN_MASK);
		//pin_toggle(LEDX2_PIO, LEDX2_PIN_MASK);
		//pin_toggle(LEDX3_PIO, LEDX3_PIN_MASK);
}

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE, handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 2);
};

void write_freq(uint32_t freq){
	uint8_t stingLCD[256];
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(10, 200, ILI9488_LCD_WIDTH-1, 215);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(stingLCD, "Frequencia: %dHz", freq);
	ili9488_draw_string(10, 200, stingLCD);
}

void but_callback(void){
	but_flag = true;
}

void pisca_led(void){
	//Set LED
	pio_set(PIOC, LED_PIO_PIN_MASK);
	delay_ms(200);
	
	//Clear LED
	pio_clear(PIOC, LED_PIO_PIN_MASK);
	delay_ms(200);
}
/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */
int main(void)
{	
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	
	pmc_enable_periph_clk(ID_PIOA);
	pmc_enable_periph_clk(ID_PIOB);
	pmc_enable_periph_clk(ID_PIOC);
	pmc_enable_periph_clk(ID_PIOD);
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);
	
	//Inicializa Bot�o da SAME70
	BUTX_init(BUT_PIO, BUT_PIO_ID, BUT_PIO_PIN, BUT_PIO_PIN_MASK, but_callback);
	
	//Inicializa botao da Xplained Pro
	BUTX_init(BUTX1_PIO, BUTX1_PIO_ID, BUTX1_PIN, BUTX1_PIN_MASK, ButtonX_Handler);
	

    /* Inicializa e configura o LCD */
	configure_lcd();
	
	pio_configure(LED_PIO, PIO_OUTPUT_0, LED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(LEDX1_PIO, PIO_OUTPUT_0, LEDX1_PIN_MASK, PIO_DEFAULT);
	pio_configure(LEDX2_PIO, PIO_OUTPUT_0, LEDX2_PIN_MASK, PIO_DEFAULT);
	pio_configure(LEDX3_PIO, PIO_OUTPUT_0, LEDX3_PIN_MASK, PIO_DEFAULT);
	
	
	TC_init(TC0, ID_TC1, 1, freq);
	
	
	
    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(stingLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	
	sprintf(stingLCD, "Frequencia: %dHz", freq);
	ili9488_draw_string(10, 200, stingLCD);
			
	while (1) {
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		if(but_flag){
			pisca_led();
			freq -= 1;
			printf("Current Frequency: %d\n", freq);
			write_freq(freq);
			TC_init(TC0, ID_TC1, 1, freq);
			but_flag = false;
		}
		
		if(butX_flag){
			pisca_led();
			freq += 1;
			printf("Current Frequency: %d\n", freq);
			write_freq(freq);
			TC_init(TC0, ID_TC1, 1, freq);
			butX_flag = false;
		}
	}
	return 0;
}
