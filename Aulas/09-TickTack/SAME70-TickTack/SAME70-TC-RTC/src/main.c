#include "asf.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/

/**
 *  Informacoes para o RTC
 *  poderia ser extraida do __DATE__ e __TIME__
 *  ou ser atualizado pelo PC.
 */
#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0

/**
* LEDs
*/
#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   8
#define LED_PIN_MASK   (1<<LED_PIN)

/**
* Bot�o
*/
#define BUT_PIO_ID			  ID_PIOA
#define BUT_PIO				  PIOA
#define BUT_PIN				  11
#define BUT_PIN_MASK		(1 << BUT_PIN)
#define BUT_DEBOUNCING_VALUE  79

#define LEDX1_PIO_ID ID_PIOA
#define LEDX1_PIO PIOA
#define LEDX1_PIN 0
#define LEDX1_PIN_MASK (1 << LEDX1_PIN)

#define LEDX2_PIO_ID ID_PIOC
#define LEDX2_PIO PIOC
#define LEDX2_PIN 30
#define LEDX2_PIN_MASK (1 << LEDX2_PIN)

#define LEDX3_PIO_ID ID_PIOB
#define LEDX3_PIO PIOB
#define LEDX3_PIN 2
#define LEDX3_PIN_MASK (1 << LEDX3_PIN)

#define BUTX1_PIO_ID ID_PIOD
#define BUTX1_PIO PIOD
#define BUTX1_PIN 28
#define BUTX1_PIN_MASK (1 << BUTX1_PIN)

#define BUTX2_PIO_ID ID_PIOC
#define BUTX2_PIO PIOC
#define BUTX2_PIN 31
#define BUTX2_PIN_MASK (1 << BUTX2_PIN)

#define BUTX3_PIO_ID ID_PIOA
#define BUTX3_PIO PIOA
#define BUTX3_PIN 19
#define BUTX3_PIN_MASK (1 << BUTX3_PIN)

/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint8_t flag_led0 = 1;
volatile uint8_t flag_ledX1 = 1;
volatile uint8_t flag_ledX2 = 1;
volatile uint8_t flag_ledX3 = 1;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/

void BUT_init(void);
void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void));
void LED_init(int estado);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);
void pin_toggle2(Pio *pio, uint32_t mask);

/************************************************************************/
/* Handlers                                                             */
/************************************************************************/

/**
*  Handle Interrupcao botao 1
*/
static void Button1_Handler(uint32_t id, uint32_t mask)
{

}

void ButtonX1_Handler(void){
	//flag = !flag;
	flag_ledX1 = !flag_ledX1; 
}

void ButtonX2_Handler(void){
	//flag = !flag;
	flag_ledX2 = !flag_ledX2;
}

void ButtonX3_Handler(void){
	//flag = !flag;
	flag_ledX3 = !flag_ledX3;
}

/**
*  Interrupt handler for TC1 interrupt.
*/
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_ledX1){
		pin_toggle(LEDX1_PIO, LEDX1_PIN_MASK);	
	}
}

void TC1_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_led0)
		pin_toggle(LED_PIO, LED_PIN_MASK);
}

void TC4_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC1, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	//pio_clear(LEDX1_PIO, LEDX1_PIN_MASK);
	if(flag_ledX2)
		pin_toggle(LEDX2_PIO, LEDX2_PIN_MASK);
}

void TC7_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC2, 1);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	//pio_clear(LEDX1_PIO, LEDX1_PIN_MASK);
	if (flag_ledX3)
		pin_toggle(LEDX3_PIO, LEDX3_PIN_MASK);
}

/**
* \brief Interrupt handler for the RTC. Refresh the display.
*/
void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
			rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
			
			flag_led0 = !flag_led0;
			
			if(flag_led0){
				pio_clear(LED_PIO, LED_PIN_MASK);
				pio_clear(LEDX1_PIO, LEDX1_PIN_MASK);
				pio_clear(LEDX2_PIO, LEDX2_PIN_MASK);
				pio_clear(LEDX3_PIO, LEDX3_PIN_MASK);
				pmc_enable_periph_clk(ID_TC0);
				pmc_enable_periph_clk(ID_TC1);
				pmc_enable_periph_clk(ID_TC4);
				pmc_enable_periph_clk(ID_TC7);
			}
			else{
				pio_set(LED_PIO, LED_PIN_MASK);
				pio_set(LEDX1_PIO, LEDX1_PIN_MASK);
				pio_set(LEDX2_PIO, LEDX2_PIN_MASK);
				pio_set(LEDX3_PIO, LEDX3_PIN_MASK);
				pmc_disable_periph_clk(ID_TC1);
				pmc_disable_periph_clk(ID_TC0);
				pmc_disable_periph_clk(ID_TC4);
				pmc_disable_periph_clk(ID_TC7);
			}
			
			uint32_t h, m, s;
			rtc_get_time(RTC, &h, &m, &s);
			rtc_set_time_alarm(RTC, 1, h, 1, m+1, 1, s);
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}


/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

/**
* @Brief Inicializa o pino do BUT
*/
void BUT_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT_PIO, BUT_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
};

void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE, handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 2);
};

/**
* @Brief Inicializa o pino do LED
*/
void LED_init(int estado){
	pio_set_output(LED_PIO, LED_PIN_MASK, estado, 0, 0 );
	pio_set_output(LEDX1_PIO, LEDX1_PIN_MASK, 0, 0, 0);
	pio_set_output(LEDX2_PIO, LEDX2_PIN_MASK, 0, 0, 0);
	pio_set_output(LEDX3_PIO, LEDX3_PIN_MASK, 0, 0, 0);
};

/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

/**
* Configura o RTC para funcionar com interrupcao de alarme
*/
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN);

}

/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	pmc_enable_periph_clk(PIOA);
	pmc_enable_periph_clk(PIOB);
	pmc_enable_periph_clk(PIOC);
	pmc_enable_periph_clk(PIOD);

	/* Configura Leds */
	LED_init(0);

	/* Configura os bot�es */
	BUT_init();
	//BUTX_init(BUTX1_PIO, BUTX1_PIO_ID, BUTX1_PIN, BUTX1_PIN_MASK, flag_ledX1, ButtonX_Handler);
	BUTX_init(BUTX1_PIO, BUTX1_PIO_ID, BUTX1_PIN, BUTX1_PIN_MASK, ButtonX1_Handler);
	BUTX_init(BUTX2_PIO, BUTX2_PIO_ID, BUTX2_PIN, BUTX2_PIN_MASK, ButtonX2_Handler);
	BUTX_init(BUTX3_PIO, BUTX3_PIO_ID, BUTX3_PIN, BUTX3_PIN_MASK, ButtonX3_Handler);
	//BUTX_init(BUTX2_PIO, BUTX2_PIO_ID, BUTX2_PIN, BUTX2_PIN_MASK);

	/** Configura RTC */
	RTC_init();

	/* configura alarme do RTC */
	rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
	rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE+1, 1, SECOND);
	
	/** Configura timer TC0, canal 1 */
	TC_init(TC0, ID_TC1, 1, 4);
	TC_init(TC0, ID_TC0, 0, 8);
	TC_init(TC1, ID_TC4, 1, 11);
	TC_init(TC2, ID_TC7, 1, 17);


	while (1) {
		/* Entrar em modo sleep */
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);

	}

}
