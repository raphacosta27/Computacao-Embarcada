/* 12- ADC
* Rafael Corsi @ insper.edu.br
* Abril 2017
*
* Configura o ADC do SAME70 para fazer leitura
* do sensor de temperatura interno
*/

/************************************************************************/
/* Includes                                                             */
/************************************************************************/


#include "asf.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "ioport.h"
#include "logo.h"

/************************************************************************/
/* Defines                                                              */
/************************************************************************/

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
	"-- "BOARD_NAME " --"STRING_EOL	\
	"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

struct ili9488_opt_t g_ili9488_display_opt;

/** Reference voltage for AFEC,in mv. */
#define VOLT_REF        (3300)

/** The maximal digital value */
/** 2^12 - 1                  */
#define MAX_DIGITAL     (4095UL)

/** The conversion data is done flag */
volatile bool is_conversion_done = false;

/** The conversion data value */
volatile uint32_t g_ul_value = 0;

/* Canal do sensor de temperatura */
#define AFEC_CHANNEL_TEMP_SENSOR 11


/** The conversion data for potenciometer is done flag */
volatile bool is_conversion_pot_done = false;

/** The conversion for potenciometer data value */
volatile uint32_t g_ul_value_pot = 0;

/* Canal da leitura do valor do potenciometro */
#define AFEC_CHANNEL_POT_SENSOR 8

/************************************************************************/
/* Callbacks / Handler                                                 */
/************************************************************************/

/**
 * \brief AFEC interrupt callback function.
 */
static void AFEC_Temp_callback(void)
{
	g_ul_value = afec_channel_get_value(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
	is_conversion_done = true;
}

static void AFEC_Pot_callback(void)
{
	g_ul_value_pot = afec_channel_get_value(AFEC0, AFEC_CHANNEL_POT_SENSOR);
	is_conversion_pot_done = true;
}

/**
}
/**
 * \brief Configure UART console.
 */

/**
 * \brief Configure UART console.
 * BaudRate : 115200
 * 8 bits
 * 1 stop bit
 * sem paridade
 */

/** 
 * converte valor lido do ADC para temperatura em graus celsius
 * input : ADC reg value
 * output: Temperature in celsius
 */
static int32_t convert_adc_to_temp(int32_t ADC_value){
  
  int32_t ul_vol;
  int32_t ul_temp;
  
	ul_vol = ADC_value * VOLT_REF / MAX_DIGITAL;

  /*
   * According to datasheet, The output voltage VT = 0.72V at 27C
   * and the temperature slope dVT/dT = 2.33 mV/C
   */
  ul_temp = (ul_vol - 720)  * 100 / 233 + 27;
  return(ul_temp);
}

static int32_t convert_adc_to_res(int32_t ADC_value){
  
  int32_t ul_vol;
  int32_t ul_res;
  
	ul_vol = ADC_value * VOLT_REF / MAX_DIGITAL;

  /*
   * According to datasheet, The output voltage VT = 0.72V at 27C
   * and the temperature slope dVT/dT = 2.33 mV/C
   */
  ul_res = (-ul_vol * 1000)/(ul_vol - 3.3);
  return(ul_res);
}

static void config_ADC_TEMP(void){
/************************************* 
   * Ativa e configura AFEC
   *************************************/  
  /* Ativa AFEC - 0 */
	afec_enable(AFEC0);

	/* struct de configuracao do AFEC */
	struct afec_config afec_cfg;

	/* Carrega parametros padrao */
	afec_get_config_defaults(&afec_cfg);

	/* Configura AFEC */
	afec_init(AFEC0, &afec_cfg);
  
	/* Configura trigger por software */
	afec_set_trigger(AFEC0, AFEC_TRIG_SW);
  
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_11,	AFEC_Temp_callback, 1); 
   
	/*** Configuracao espec�fica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, &afec_ch_cfg);
  
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_TEMP_SENSOR, 0x200);

	/***  Configura sensor de temperatura ***/
	struct afec_temp_sensor_config afec_temp_sensor_cfg;

	afec_temp_sensor_get_config_defaults(&afec_temp_sensor_cfg);
	afec_temp_sensor_set_config(AFEC0, &afec_temp_sensor_cfg);

	/* Selecina canal e inicializa convers�o */  
	afec_channel_enable(AFEC0, AFEC_CHANNEL_TEMP_SENSOR);
}

static void config_ADC_POT(void){
	/* configura call back */
	afec_set_callback(AFEC0, AFEC_INTERRUPT_EOC_8, AFEC_Pot_callback, 1);
	
	/*** Configuracao espec�fica do canal AFEC ***/
	struct afec_ch_config afec_ch_cfg;
	afec_ch_get_config_defaults(&afec_ch_cfg);
	afec_ch_cfg.gain = AFEC_GAINVALUE_0;
	afec_ch_set_config(AFEC0, AFEC_CHANNEL_POT_SENSOR, &afec_ch_cfg);
	
	/*
	* Calibracao:
	* Because the internal ADC offset is 0x200, it should cancel it and shift
	 down to 0.
	 */
	//afec_channel_set_analog_offset(AFEC0, AFEC_CHANNEL_POT_SENSOR, 0x200);
	
	/* Selecina canal e inicializa convers�o */
	afec_channel_enable(AFEC0, AFEC_CHANNEL_POT_SENSOR);
}

static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate =		CONF_UART_BAUDRATE,
		.charlength =	CONF_UART_CHAR_LENGTH,
		.paritytype =	CONF_UART_PARITY,
		.stopbits =		CONF_UART_STOP_BITS,
	};

	/* Configure UART console. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

void write_temp(uint32_t freq){
	uint8_t stingLCD[256];
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(10, 200, ILI9488_LCD_WIDTH-1, 215);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(stingLCD, "Temperatura: %doC", freq);
	ili9488_draw_string(10, 200, stingLCD);
}

void write_res(uint32_t res){
	uint8_t stingLCD[256];
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(10, 220, ILI9488_LCD_WIDTH-1, 235);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	sprintf(stingLCD, "Resistencia: %dOhm", res);
	ili9488_draw_string(10, 220, stingLCD);
}
/**
 * \brief Main application function.
 *
 * Initialize system, UART console, network then start weather client.
 *
 * \return Program return value.
 */
int main(void)
{
	// array para escrita no LCD
	uint8_t stingLCD[256];
	
	/* Initialize the board. */
	sysclk_init();
	board_init();
	ioport_init();
	
	/* inicializa delay */
	delay_init(sysclk_get_cpu_hz());
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);
	
	/* inicializa e configura adc */
	config_ADC_TEMP();
	config_ADC_POT();

    /* Inicializa e configura o LCD */
	configure_lcd();
	
	/* Output example information. */
	puts(STRING_HEADER);
	

    /* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	
	sprintf(stingLCD, "Computacao Embarcada %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	
	afec_start_software_conversion(AFEC0);
	while (1) {
		if(is_conversion_done == true) {
			is_conversion_done = false;
			uint32_t temp = convert_adc_to_temp(g_ul_value);
			printf("Temp : %d \r\n", temp);
			write_temp(temp);
			afec_start_software_conversion(AFEC0);
			delay_s(1);
		}
		if(is_conversion_pot_done == true) {
			is_conversion_pot_done = false;
			uint32_t res = convert_adc_to_res(g_ul_value_pot);
			printf("RES : %d \r\n", g_ul_value_pot);
			write_res(res);
			//afec_start_software_conversion(AFEC0);
			//delay_s(1);
		}
	}
	return 0;
}
