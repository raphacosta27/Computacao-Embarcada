/**
 * 5 semestre - Eng. da Computa��o - Insper
 * Rafael Corsi - rafael.corsi@insper.edu.br
 *
 * Projeto 0 para a placa SAME70-XPLD
 *
 * Objetivo :
 *  - Introduzir ASF e HAL
 *  - Configuracao de clock
 *  - Configuracao pino In/Out
 *
 * Material :
 *  - Kit: ATMEL SAME70-XPLD - ARM CORTEX M7
 */

#include "asf.h"
#include "conf_board.h"
#include <string.h>
#include <stdlib.h>

#define USART_COM_ID ID_USART1
#define USART_COM    USART1

#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      1
#define SECOND      0

/************************************************************************/
/* defines                                                              */
/************************************************************************/

#define LED_PIO PIOA
#define LED_PIO_ID ID_PIOA
#define LED_PIO_PIN 19
#define LED_PIO_PIN_MASK ( 1 << LED_PIO_PIN)

/************************************************************************/
/* constants                                                            */
/************************************************************************/

/************************************************************************/
/* variaveis globais                                                    */
/************************************************************************/

volatile uint32_t contador = 0;
volatile uint32_t is_toggling = 0;
/************************************************************************/
/* interrupcoes                                                         */
/************************************************************************/

/************************************************************************/
/* funcoes                                                              */
/************************************************************************/

void pin_toggle(Pio *pio, uint32_t mask);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init();

/************************************************************************/
/* Main                                                                 */
/************************************************************************/

/**
 * \brief Configure the console UART.
 */
static void configure_console(void){
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
	#if (defined CONF_UART_CHAR_LENGTH)
			.charlength = CONF_UART_CHAR_LENGTH,
	#endif
			.paritytype = CONF_UART_PARITY,
	#if (defined CONF_UART_STOP_BITS)
			.stopbits = CONF_UART_STOP_BITS,
	#endif
		};

		/* Configure console UART. */
		stdio_serial_init(CONF_UART, &uart_serial_options);

		/* Specify that stdout should not be buffered. */
	#if defined(__GNUC__)
		setbuf(stdout, NULL);
	#else
		/* Already the case in IAR's Normal DLIB default configuration: printf()
		 * emits one character at a time.
		 */
	#endif
}




void pisca_led(){
	// Coloca 0 no pino do LED
	pio_clear(LED_PIO, LED_PIO_PIN_MASK);
	delay_ms(200);
	
	pio_set(LED_PIO, LED_PIO_PIN_MASK);
	delay_ms(200);
}

void USART1_Handler(void){
	uint32_t ret = usart_get_status(USART_COM);

	//BaseType_t xHigherPriorityTaskWoken = pdTRUE;
	char c;
	
	// Verifica por qual motivo entrou na interrup�cao
	//  - Dadodispon�vel para leitura
	//printf("Flag: %i", ret & US_IER_RXRDY);
	if(ret & US_IER_RXRDY){
		//printf("Flag RCRDY � 1\n");
		usart_serial_getchar(USART_COM, &c);
		//printf("GetChar call");
		//while(c != 'NULL'){
		//usart_serial_putchar(USART_COM, c);
		//xQueueSend(xQueueUsartRx, (void *) c, 10);
		//}
		printf("%c", c);
		//printf("%c",c);
		// -  Transmissoa finalizada
		} else if(ret & US_IER_TXRDY){
		printf("Finalizou msg");
		return;
	}
	
}

//void TC1_Handler(void){
	//volatile uint32_t ul_dummy;
//
	///****************************************************************
	//* Devemos indicar ao TC que a interrup��o foi satisfeita.
	//******************************************************************/
	//ul_dummy = tc_get_status(TC0, 1);
//
	///* Avoid compiler warning */
	//UNUSED(ul_dummy);
//
	///** Muda o estado do LED */
	//if(flag_led0)
		//pin_toggle(LED_PIO, LED_PIO_PIN_MASK);
//}

void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	//if(contador < 8){
		//pin_toggle(LED_PIO, LED_PIO_PIN_MASK);
		//contador ++;
	//}
	//else if( contador == 8){
		//contador = 0;
		//pio_set(LED_PIO, LED_PIO_PIN_MASK);
		//pmc_disable_periph_clk(ID_TC0);
	//}
	pin_toggle(LED_PIO, LED_PIO_PIN_MASK);
	contador ++;
	
}


void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	
	uint32_t h, m, s;
	rtc_get_time(RTC, &h, &m, &s);
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
			rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
			is_toggling = 1;
			rtc_set_time_alarm(RTC, 1, h, 1, m, 1, s+3);
			pmc_enable_periph_clk(TC0);
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}


void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN);

}


void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

static void USART1_init(void){
	/* Configura USART1 Pinos */
	sysclk_enable_peripheral_clock(ID_PIOB);
	sysclk_enable_peripheral_clock(ID_PIOA);
	
	pio_set_peripheral(PIOB, PIO_PERIPH_D, PIO_PB4); // RX
	pio_set_peripheral(PIOA, PIO_PERIPH_A, PIO_PA21); // TX
	MATRIX->CCFG_SYSIO |= CCFG_SYSIO_SYSIO4;

	/* Configura opcoes USART */
	const sam_usart_opt_t usart_settings = {
		.baudrate       = 115200,
		.char_length    = US_MR_CHRL_8_BIT,
		.parity_type    = US_MR_PAR_NO,
		.stop_bits   	= US_MR_NBSTOP_1_BIT,
		.channel_mode   = US_MR_CHMODE_NORMAL
	};

	/* Ativa Clock periferico USART0 */
	sysclk_enable_peripheral_clock(USART_COM_ID);

	/* Configura USART para operar em modo RS232 */
	usart_init_rs232(USART_COM, &usart_settings, sysclk_get_peripheral_hz());

	/* Enable the receiver and transmitter. */
	usart_enable_tx(USART_COM);
	usart_enable_rx(USART_COM);

	/* map printf to usart */
	ptr_put = (int (*)(void volatile*,char))&usart_serial_putchar;
	ptr_get = (void (*)(void volatile*,char*))&usart_serial_getchar;

	/* ativando interrupcao */
	usart_enable_interrupt(USART_COM, US_IER_RXRDY);
	NVIC_SetPriority(USART_COM_ID, 5);
	NVIC_EnableIRQ(USART_COM_ID);

}



// Funcao principal chamada na inicalizacao do uC.
int main(void){
	// Initialize the board clock
	sysclk_init();
	board_init();
		
	// Disativa WatchDog
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	// Ativa  o periferico responsavel pelo controle do LED
	pmc_enable_periph_clk(LED_PIO_ID);
	pmc_enable_periph_clk(ID_RTC);
	
	/** Configura RTC */
	RTC_init();
	
	configure_console();
	USART1_init();
	
	TC_init(TC0, ID_TC0, 0, 20);
	
	//printf("inicializando \n");

	
	// Disables backup SRAM
	SUPC->SUPC_MR = SUPC_MR_ONREG;

	/* configura alarme do RTC */
	rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
	rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE, 1, SECOND+3);
	
	// Inicializa PC8 como sa�da
	pio_configure(LED_PIO, PIO_OUTPUT_1, LED_PIO_PIN_MASK, PIO_DEFAULT);
	
	pmc_set_fast_startup_input(PMC_FSMR_RTCAL);
	supc_set_wakeup_mode(SUPC, SUPC_WUMR_RTCEN_ENABLE);
		
	while (1) {
		//printf("alo");
		if(is_toggling){
			pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
			if( contador == 8){
				contador = 0;
				is_toggling = 0;
				//pio_set(LED_PIO, LED_PIO_PIN_MASK);
				pmc_disable_periph_clk(TC0);
			}
		}
		else{
			/*pmc_sleep(SAM_PM_SMODE_BACKUP);*/
			pmc_sleep(SAM_PM_SMODE_SLEEP_WFI);
		}
		//delay_ms(500);
	}
	return 0;
}
