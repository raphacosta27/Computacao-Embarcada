/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include "asf.h"
#include "stringz.h"
#include "stdio_serial.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "ioport.h"
#include "logo.h"

#define STRING_EOL    "\r\n"
#define STRING_HEADER "-- SAME70 LCD DEMO --"STRING_EOL	\
		"-- "BOARD_NAME " --"STRING_EOL	\
		"-- Compiled: "__DATE__ " "__TIME__ " --"STRING_EOL

struct ili9488_opt_t g_ili9488_display_opt;

#define PIN_KEYBOARD_COLUMN_1_PIO   PIOA
#define PIN_KEYBOARD_COLUMN_1_MASK  (1u << 0)
#define PIN_KEYBOARD_COLUMN_2_PIO   PIOB
#define PIN_KEYBOARD_COLUMN_2_MASK  (1u << 3)
#define PIN_KEYBOARD_COLUMN_3_PIO   PIOC
#define PIN_KEYBOARD_COLUMN_3_MASK  (1u << 31)
#define PIN_KEYBOARD_LINE_1_PIO   PIOD
#define PIN_KEYBOARD_LINE_1_MASK  (1u << 25)
#define PIN_KEYBOARD_LINE_2_PIO   PIOB
#define PIN_KEYBOARD_LINE_2_MASK  (1u << 0)
#define PIN_KEYBOARD_LINE_3_PIO   PIOA
#define PIN_KEYBOARD_LINE_3_MASK  (1u << 3)
#define PIN_KEYBOARD_LINE_4_PIO   PIOD
#define PIN_KEYBOARD_LINE_4_MASK  (1u << 28)

#define BUZZER_PIO PIOD
#define BUZZER_PIO_ID ID_PIOD
#define BUZZER_PIO_PIN 22
#define BUZZER_PIO_PIN_MASK (1 << BUZZER_PIO_PIN) 

#define GREENLED_PIO PIOD
#define GREENLED_PIO_ID ID_PIOD
#define GREENLED_PIO_PIN 21
#define GREENLED_PIO_PIN_MASK (1 << GREENLED_PIO_PIN)

#define REDLED_PIO PIOB
#define REDLED_PIO_ID ID_PIOB
#define REDLED_PIO_PIN 1
#define REDLED_PIO_PIN_MASK (1 << REDLED_PIO_PIN)


static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
		#ifdef CONF_UART_CHAR_LENGTH
		.charlength = CONF_UART_CHAR_LENGTH,
		#endif
		.paritytype = CONF_UART_PARITY,
		#ifdef CONF_UART_STOP_BITS
		.stopbits = CONF_UART_STOP_BITS,
		#endif
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}

static void configure_lcd(void){
	/* Initialize display parameter */
	g_ili9488_display_opt.ul_width = ILI9488_LCD_WIDTH;
	g_ili9488_display_opt.ul_height = ILI9488_LCD_HEIGHT;
	g_ili9488_display_opt.foreground_color = COLOR_CONVERT(COLOR_WHITE);
	g_ili9488_display_opt.background_color = COLOR_CONVERT(COLOR_WHITE);

	/* Initialize LCD */
	ili9488_init(&g_ili9488_display_opt);
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, ILI9488_LCD_HEIGHT-1);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_TOMATO));
	ili9488_draw_filled_rectangle(0, 0, ILI9488_LCD_WIDTH-1, 120-1);
	ili9488_draw_filled_rectangle(0, 360, ILI9488_LCD_WIDTH-1, 480-1);
	ili9488_draw_pixmap(0, 50, 319, 129, logoImage);
	
}

volatile char input_array[4];

void buzz(){
	pio_clear(BUZZER_PIO, BUZZER_PIO_PIN_MASK);
	for(int i = 0; i < 200; i++) {
		pio_set(BUZZER_PIO, BUZZER_PIO_PIN_MASK);
		delay_us(500);
		pio_clear(BUZZER_PIO, BUZZER_PIO_PIN_MASK);
		delay_us(500);
	}
}

void toggle_led(Pio *pio, uint32_t ul_mask, int status){
	if(status){
		pio_clear(pio, ul_mask);	
	}
	else{
		pio_set(pio, ul_mask);
	}
}

//char set_button(char key, char* pressed)
//{	
	//*pressed = key;
	//printf("Pressed key %c\r\n ", key);
	//delay_ms(500);
	//*pressed = ' ';
	//return key;
//}

char listen_keyboard(char *pressed) {
	pio_clear(PIN_KEYBOARD_COLUMN_1_PIO, PIN_KEYBOARD_COLUMN_1_MASK); // habilita coluna 1
	// varre linhas
	if(!pio_get(PIN_KEYBOARD_LINE_1_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_1_MASK)) {
		return '1';
		//puts("Coluna 1 Linha 1\r\n");
	}
	if(!pio_get(PIN_KEYBOARD_LINE_2_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_2_MASK)) {
		return '4';
		//puts("Coluna 1 Linha 2\r\n");
	}
	if(!pio_get(PIN_KEYBOARD_LINE_3_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_3_MASK)) {
		//puts("Coluna 1 Linha 3\r\n");
		return '7';
	}
	if(!pio_get(PIN_KEYBOARD_LINE_4_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_4_MASK)) {
		return '*';
		//puts("Coluna 1 Linha 4\r\n");
	}
	pio_set(PIN_KEYBOARD_COLUMN_1_PIO, PIN_KEYBOARD_COLUMN_1_MASK);
	
	
	pio_clear(PIN_KEYBOARD_COLUMN_2_PIO, PIN_KEYBOARD_COLUMN_2_MASK); // habilita coluna 2
	// varre linhas
	if(!pio_get(PIN_KEYBOARD_LINE_1_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_1_MASK)) {
		//puts("Coluna 2 Linha 1\r\n");
		return '2';
	}
	if(!pio_get(PIN_KEYBOARD_LINE_2_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_2_MASK)) {
		//puts("Coluna 2 Linha 2\r\n");
		return '5';
	}
	if(!pio_get(PIN_KEYBOARD_LINE_3_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_3_MASK)) {
		//puts("Coluna 2 Linha 3\r\n");
		return '8';
	}
	if(!pio_get(PIN_KEYBOARD_LINE_4_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_4_MASK)) {
		//puts("Coluna 2 Linha 4\r\n");
		return '0';
	}
	pio_set(PIN_KEYBOARD_COLUMN_2_PIO, PIN_KEYBOARD_COLUMN_2_MASK);
	
	
	pio_clear(PIN_KEYBOARD_COLUMN_3_PIO, PIN_KEYBOARD_COLUMN_3_MASK); // habilita coluna 3
	// varre linhas
	if(!pio_get(PIN_KEYBOARD_LINE_1_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_1_MASK)) {
		return '3';
		//puts("Coluna 3 Linha 1\r\n");
	}
	if(!pio_get(PIN_KEYBOARD_LINE_2_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_2_MASK)) {
		//puts("Coluna 3 Linha 2\r\n");
		return '6';
	}
	if(!pio_get(PIN_KEYBOARD_LINE_3_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_3_MASK)) {
		//puts("Coluna 3 Linha 3\r\n");
		return '9';
	}
	if(!pio_get(PIN_KEYBOARD_LINE_4_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_4_MASK)) {
		//puts("Coluna 3 Linha 4\r\n");
		return '#';
	}
	pio_set(PIN_KEYBOARD_COLUMN_3_PIO, PIN_KEYBOARD_COLUMN_3_MASK);
	return 'x';
}

void print_password(char arr[4]){
	printf("\n ======> CURRENT PASSWORD : ");
	for(int i = 0; i < 4; i++){
		printf("%c",arr[i]);
	}
	printf("\n");
}

int check_password(char input[], char password[])
{
	for(int i = 0; i < 4; i++) {
		if (input[i] != password[i]) {
			return 0;
		}
	}
	return 1;
}

void clear_array(char arr[]){
	for(int i = 0; i < 4; i++) {
		arr[i] = 0;
	}
}

int main (void)
{
	char pressed = ' ';
	char key = 'x';
	char state = 'i';
	char input[4];
	char password[4] = "1234";
	short input_count = 0;
	int is_open = 0;
	uint8_t stingLCD[256];
	/* Insert system clock initialization code here (sysclk_init()). */
	
	sysclk_init();
	board_init();
	ioport_init();
	delay_init();
	
	/* Initialize the UART console. */
	configure_console();
	printf(STRING_HEADER);
	
	pmc_enable_periph_clk(ID_USART1);
	
	/* Inicializa e configura o LCD */
	configure_lcd();
	
	/* Escreve na tela Computacao Embarcada 2018 */
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_WHITE));
	ili9488_draw_filled_rectangle(0, 300, ILI9488_LCD_WIDTH-1, 315);
	ili9488_set_foreground_color(COLOR_CONVERT(COLOR_BLACK));
	 
	sprintf(stingLCD, "Computacao ALOALO %d", 2018);
	ili9488_draw_string(10, 300, stingLCD);
	
 	pmc_enable_periph_clk(ID_PIOA);
 	pmc_enable_periph_clk(ID_PIOB);
 	pmc_enable_periph_clk(ID_PIOC);
 	pmc_enable_periph_clk(ID_PIOD);
	
    pio_configure(PIN_KEYBOARD_LINE_1_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_1_MASK, PIO_PULLUP);
	pio_configure(PIN_KEYBOARD_LINE_2_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_2_MASK, PIO_PULLUP);
	pio_configure(PIN_KEYBOARD_LINE_3_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_3_MASK, PIO_PULLUP);
	pio_configure(PIN_KEYBOARD_LINE_4_PIO, PIO_INPUT, PIN_KEYBOARD_LINE_4_MASK, PIO_PULLUP);
	pio_configure(PIN_KEYBOARD_COLUMN_1_PIO, PIO_OUTPUT_1, PIN_KEYBOARD_COLUMN_1_MASK, PIO_DEFAULT);
	pio_configure(PIN_KEYBOARD_COLUMN_2_PIO, PIO_OUTPUT_1, PIN_KEYBOARD_COLUMN_2_MASK, PIO_DEFAULT);
	pio_configure(PIN_KEYBOARD_COLUMN_3_PIO, PIO_OUTPUT_1, PIN_KEYBOARD_COLUMN_3_MASK, PIO_DEFAULT);
	
	pio_configure(BUZZER_PIO, PIO_OUTPUT_0, BUZZER_PIO_PIN_MASK, PIO_DEFAULT);
	
	pio_configure(GREENLED_PIO, PIO_OUTPUT_1, GREENLED_PIO_PIN_MASK, PIO_DEFAULT);
	pio_configure(REDLED_PIO, PIO_OUTPUT_1, REDLED_PIO_PIN_MASK, PIO_DEFAULT);
	
	
	puts("Configuracao completa\n");


	while(1) {
		putchar(12);
		if(is_open){
			toggle_led(REDLED_PIO, REDLED_PIO_PIN_MASK, 0);
			toggle_led(GREENLED_PIO, GREENLED_PIO_PIN_MASK, 1);
		}
		else{
			toggle_led(GREENLED_PIO, GREENLED_PIO_PIN_MASK, 0);
			toggle_led(REDLED_PIO, REDLED_PIO_PIN_MASK, 1);
		}
		key = listen_keyboard(&pressed);
		
		if (key != 'x') {
			buzz();
		}
		
		printf("------ STATE : %c KEY : %c ------\n",state,key);
		
		if(state == 'i'){
			clear_array(input);
			printf("Initial state\n");
			printf("Press 1 to set a password\n");
			printf("Press 2 to view the password\n");
			printf("Press 3 to try opening the safe\n");
			printf("Press 4 to close the safe\n");
			
			printf("Current input %c\n",key);
			if (key == '1') {
				state = 's';
			}
			if (key == '2') {
				print_password(password);
			}
			if (key == '3') {
				state = 'o';	
			}
			if (key == '4') {
				is_open = 0;
				buzz();
			}
			
			
		} else if(state == 's') {
			
			printf("Setting password state\n");
			print_password(input);
			if (key != 'x' && key != '#') {
				input[input_count] = key;
				input_count++;
			} 
			if (key == '#') {
				print_password(input);
				input_count = 0;
				state = 'i';
				strcpy(password,input);
			}
			
		} else if (state == 'o') {
			printf("Opening state\n");
			if (key != 'x' && key != '#') {
				input[input_count] = key;
				input_count++;
				print_password(input);
			}
			if (key == '#') {
				print_password(input);
				input_count = 0;
				if (check_password(input,password)) {
					buzz();
					delay_ms(200);
					buzz();
					printf("THATS RIGHT MA NIGGA\n");
					is_open = 1;
				} else {
					buzz();
					printf("WRONG AS SHIT THO\n");	
					is_open = 0;
				}
				state = 'i';
			}
			
		}
		
		
		delay_ms(300);
				
	}

}
