/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>

#define YEAR        2018
#define MOUNTH      4
#define DAY         9
#define WEEK        15
#define HOUR		10
#define MINUTE      13
#define SECOND      0

#define BUT_PIO_ID			  ID_PIOA
#define BUT_PIO				  PIOA
#define BUT_PIN				  11
#define BUT_PIN_MASK		(1 << BUT_PIN)

#define LEDX1_PIO_ID ID_PIOA
#define LEDX1_PIO PIOA
#define LEDX1_PIN 0
#define LEDX1_PIN_MASK (1 << LEDX1_PIN)

#define LEDX2_PIO_ID ID_PIOC
#define LEDX2_PIO PIOC
#define LEDX2_PIN 30
#define LEDX2_PIN_MASK (1 << LEDX2_PIN)

#define LEDX3_PIO_ID ID_PIOB
#define LEDX3_PIO PIOB
#define LEDX3_PIN 2
#define LEDX3_PIN_MASK (1 << LEDX3_PIN)

#define BUTX1_PIO_ID ID_PIOD
#define BUTX1_PIO PIOD
#define BUTX1_PIN 28
#define BUTX1_PIN_MASK (1 << BUTX1_PIN)

#define BUTX2_PIO_ID ID_PIOC
#define BUTX2_PIO PIOC
#define BUTX2_PIN 31
#define BUTX2_PIN_MASK (1 << BUTX2_PIN)

#define BUTX3_PIO_ID ID_PIOA
#define BUTX3_PIO PIOA
#define BUTX3_PIN 19
#define BUTX3_PIN_MASK (1 << BUTX3_PIN)


#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#include "stringz.h"
#include "stdio_serial.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

static void configure_console(void)
{
	const usart_serial_options_t uart_serial_options = {
		.baudrate = CONF_UART_BAUDRATE,
		#ifdef CONF_UART_CHAR_LENGTH
		.charlength = CONF_UART_CHAR_LENGTH,
		#endif
		.paritytype = CONF_UART_PARITY,
		#ifdef CONF_UART_STOP_BITS
		.stopbits = CONF_UART_STOP_BITS,
		#endif
	};

	/* Configure console UART. */
	sysclk_enable_peripheral_clock(CONSOLE_UART_ID);
	stdio_serial_init(CONF_UART, &uart_serial_options);
}


void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void));
void LED_init();
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);
void RTC_init(void);
void pin_toggle(Pio *pio, uint32_t mask);
void get_str_rtc_hour(char *hora);

volatile uint32_t minuteSetted = 0;

volatile bool settedAlarm = false;

volatile bool butX1_pro = false;
volatile bool butX2_pro = false;

volatile bool flag_ledx1pro = false;

volatile uint32_t alarm = 0;
volatile uint32_t countdown = 0;

volatile uint32_t minuto = MINUTE;

char state = 'x';

static void Button1_Handler(uint32_t id, uint32_t mask){
	flag_ledx1pro = false;
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_CLR, GFX_WHOLE);
	pio_set(LEDX1_PIO, LEDX1_PIN_MASK);
	settedAlarm = false;
}

void ButtonX1_Handler(void){
	if(state == 'i'){
		gfx_mono_draw_filled_rect(0,0,128,32,GFX_PIXEL_CLR);
		char show_alarm[100];
		sprintf(show_alarm, "%07d", alarm);
		gfx_mono_draw_string(show_alarm, 0, 0, &sysfont);
		state = 'c';	
	}
	else if(state == 'c'){
				
		printf("ALARME CONFIGURADO PARA DAQUI %d MINUTOS", alarm);
		settedAlarm = true;
		
		uint32_t h, m, s;
		rtc_get_time(RTC, &h, &m, &s);
		
		minuteSetted = m + alarm;
		
		if(minuteSetted > 60){
			minuteSetted = minuteSetted - 60;
			h += 1;
		}
		rtc_set_time_alarm(RTC, 1, h, 1, m + alarm, 1, s);
		
		alarm = 0;
				
		gfx_mono_draw_filled_rect(0,0,128,32,GFX_PIXEL_CLR);
				
		char hora[100];
		get_str_rtc_hour(&hora);
		gfx_mono_draw_string(hora, 0, 0, &sysfont);
				
		gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
				
		state = 'i';
	}	
}

void ButtonX2_Handler(void){
	if(state ==  'c'){
		alarm += 1;
		gfx_mono_draw_filled_rect(0,0,128,32,GFX_PIXEL_CLR);
		char show_alarm[100];
		sprintf(show_alarm, "%07d", alarm);
		gfx_mono_draw_string(show_alarm, 0, 0, &sysfont);	
	}
	else{
		printf("Tecla inv�lida");
	}
}

void ButtonX3_Handler(void){
	if(state == 'i'){
		gfx_mono_draw_filled_rect(0,0,128,32,GFX_PIXEL_CLR);
		if(minuteSetted == 0){
			gfx_mono_draw_string("100ALARME", 0, 0, &sysfont);
		} 
		else{
			uint32_t h, m, s;
			rtc_get_time(RTC, &h, &m, &s);
			char current[100];
			sprintf(current, "%d:%d%c%c", h, minuteSetted, 'A', 'M');
			gfx_mono_draw_string(current, 0, 0, &sysfont);	
		}
		state = 's';
	}
	
	else if(state == 's'){
		gfx_mono_draw_filled_rect(0,0,128,32,GFX_PIXEL_CLR);
		char hora[100];
		get_str_rtc_hour(&hora);
		gfx_mono_draw_string(hora, 0, 0, &sysfont);
		
		if(settedAlarm){
			gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
		}
		state = 'i';
	}
}

void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
		
		uint32_t h, m, s;
		rtc_get_time(RTC, &h, &m, &s);
		
		if(m != minuto){
			char hora[100];
			get_str_rtc_hour(&hora);
			gfx_mono_draw_string(hora, 0, 0, &sysfont);	
			minuto = m;
		}
		
		if(flag_ledx1pro){
			pin_toggle(LEDX1_PIO, LEDX1_PIN_MASK);
		}
	}
	
	/* Time or date alarm */
	if ((ul_status & RTC_SR_ALARM) == RTC_SR_ALARM) {
			rtc_clear_status(RTC, RTC_SCCR_ALRCLR);
			
			//for(int i = 0; i < 70; i++){
				//printf("ACOOOOOOOOOOOOOOOORDA");
				//pin_toggle();
				//delay_ms(200);
			//}
			//flag_ledx1pro = true;
			printf("ALARME ATINGIDO");
			printf("LED PISCANDO");			
			flag_ledx1pro = true;
			
			alarm = 0;

			countdown = 10;
			//pio_set(LEDX1_PIO, LEDX1_PIN_MASK);
			
	}
	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
	
}

/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/


void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if(flag_ledx1pro){
		pin_toggle(LEDX1_PIO, LEDX1_PIN_MASK);	
	}
}

/**
*  Toggle pin controlado pelo PIO (out)
*/
void pin_toggle(Pio *pio, uint32_t mask){
	if(pio_get_output_data_status(pio, mask))
	pio_clear(pio, mask);
	else
	pio_set(pio,mask);
}

void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_ALREN | RTC_IER_SECEN);

}

void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE, handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 2);
};

void BUT_init(void){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(BUT_PIO_ID);
	pio_set_input(BUT_PIO, BUT_PIN_MASK, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(BUT_PIO, BUT_PIN_MASK);
	pio_handler_set(BUT_PIO, BUT_PIO_ID, BUT_PIN_MASK, PIO_IT_FALL_EDGE, Button1_Handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(BUT_PIO_ID);
	NVIC_SetPriority(BUT_PIO_ID, 1);
};

/**
* @Brief Inicializa o pino do LED
*/
void LED_init(){
	pio_set_output(LEDX1_PIO, LEDX1_PIN_MASK, 1, 0, 0);
	pio_set_output(LEDX2_PIO, LEDX2_PIN_MASK, 0, 0, 0);
	pio_set_output(LEDX3_PIO, LEDX3_PIN_MASK, 0, 0, 0);
};

void get_str_rtc_hour(char *hora){
	uint32_t h, m, s;
	rtc_get_time(RTC, &h, &m, &s);
	if(h > 12){
		sprintf(hora, "%d:%d%c%c", h, m, 'P', 'M');	
	}
	else{
		sprintf(hora, "%d:%d%c%c", h, m, 'A', 'M');	
	}
}

void configure_alarm(){
	//char hora[100];
	//get_str_rtc_hour(&hora);
	gfx_mono_draw_string("0000", 0, 0, &sysfont);
	pmc_sleep(10);
}

int main (void)
{
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();
	
	WDT->WDT_MR = WDT_MR_WDDIS;
	
	pmc_enable_periph_clk(PIOA);
	pmc_enable_periph_clk(PIOB);
	pmc_enable_periph_clk(PIOC);
	pmc_enable_periph_clk(PIOD);
	
	//Inicializa Botoes da placa
	
	
	/** Configura RTC */
	RTC_init();
	
	//Inicializa botoes
	BUT_init();
	BUTX_init(BUTX1_PIO, BUTX1_PIO_ID, BUTX1_PIN, BUTX1_PIN_MASK, ButtonX1_Handler);
	BUTX_init(BUTX2_PIO, BUTX2_PIO_ID, BUTX2_PIN, BUTX2_PIN_MASK, ButtonX2_Handler);
	BUTX_init(BUTX3_PIO, BUTX3_PIO_ID, BUTX3_PIN, BUTX3_PIN_MASK, ButtonX3_Handler);	
	
	//inicializa LEDs
	LED_init();
	
	//Configura terminal
	configure_console();
	
	/* configura alarme do RTC */
	//rtc_set_date_alarm(RTC, 1, MOUNTH, 1, DAY);
	//rtc_set_time_alarm(RTC, 1, HOUR, 1, MINUTE+1, 1, SECOND);
	
	//TC_init(TC0, ID_TC1, 1, 10);
	//pmc_disable_periph_clk(ID_TC0);

	char hora[100];
	get_str_rtc_hour(&hora);
    gfx_mono_draw_string(hora, 0, 0, &sysfont);
	state = 'i';
	
	while(1) {
		if(state == 'i'){
			printf("pressione o Bot�o 1 para configurar um alarme\n");
			printf("current state: \n", state);
		}
		if(state == 'c'){
			printf("Pressione o bot�o 2 para aumentar o alarme\n");
		}
		if(state == 's'){
			printf("Mostrando alarme\n");
			printf("current state: \n", state);
		}
		
	}
}
